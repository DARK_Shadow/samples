unit uTestClass;

interface

uses
  uDataHolder, uClassTeamBuilder;

type
  TTestClass = record
  public
    type TTestClassProperty = (pVariant, pString, pDouble, pInteger);
    const TestClassProperty: array[TTestClassProperty] of string = (
      'testVariant', 'testString', 'testDouble', 'testInteger');
  private
    FBuilder: IBuilder;

    function GetProperty(const AProperty: TTestClassProperty): IPropertyFiller;
  public
    constructor Create(const ADataHolder: IDataHolder);

    property pVariant: IPropertyFiller index pVariant read GetProperty;
    property pString: IPropertyFiller index pString read GetProperty;
    property pDouble: IPropertyFiller index pDouble read GetProperty;
    property pInteger: IPropertyFiller index pInteger read GetProperty;
  end;

implementation

{ TTestClass }

constructor TTestClass.Create(const ADataHolder: IDataHolder);
begin
  FBuilder := CreateBuilder(ADataHolder);
end;

function TTestClass.GetProperty(
  const AProperty: TTestClassProperty): IPropertyFiller;
begin
  Result := FBuilder.&Property[TestClassProperty[AProperty]];
end;

end.
