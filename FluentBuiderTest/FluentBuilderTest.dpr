program FluentBuilderTest;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  uTeam in 'uTeam.pas',
  uTeamBuilder in 'uTeamBuilder.pas',
  uClassTeamBuilder in 'uClassTeamBuilder.pas',
  uDataHolder in 'uDataHolder.pas',
  uTestClass in 'uTestClass.pas';

procedure Test_1;
var
  team: TTeam;
begin
  team := TTeamBuilder.New
    .CreateTeam('Chelsea')
    .WithNickName('The blues')
    .FromTown('London')
    .WithShirtColor(ceBlue)
    .PlayingAt('Stamford Bridge');
  try
    Writeln(team.ToString);
  finally
    team.Free;
  end;
end;

procedure Test_2;
var
  LData: IDataHolder;
begin
  LData := CreateDataHolder;

  LData.Value['testVariant'] := 'testVariant';
  LData.AsString['testString'] := 'testString';
  LData.AsDouble['testDouble'] := Pi;
  LData.AsInteger['testInteger'] := 12345;

  Writeln('testVariant: ', LData.Value['testVariant']);
  Writeln('testString: ', LData.AsString['testString']);
  Writeln('testDouble: ', LData.AsDouble['testDouble']);
  Writeln('testInteger: ', LData.AsInteger['testInteger']);
end;

procedure Test_3;
var
  LData: IDataHolder;
begin
  LData := CreateDataHolder;

  CreateBuilder(LData)
    .&Property['testVariant'].Write.SetValue('testVariant')
    .&Property['testString'].Write.SetAsString('testString')
    .&Property['testDouble'].Write.SetAsDouble(Pi)
    .&Property['testInteger'].Write.SetAsInteger(12345);

  Writeln('testVariant: ', LData.Value['testVariant']);
  Writeln('testString: ', LData.AsString['testString']);
  Writeln('testDouble: ', LData.AsDouble['testDouble']);
  Writeln('testInteger: ', LData.AsInteger['testInteger']);
end;

procedure Test_4;
var
  LData: IDataHolder;
begin
  LData := CreateDataHolder;

  with TTestClass.Create(LData) do
  begin
    pVariant.Write.SetValue('testVariant');
    pString.Write.SetAsString('testString');
    pDouble.Write.SetAsDouble(Pi);
    pInteger.Write.SetAsInteger(12345);
  end;

  Writeln('testVariant: ', LData.Value['testVariant']);
  Writeln('testString: ', LData.AsString['testString']);
  Writeln('testDouble: ', LData.AsDouble['testDouble']);
  Writeln('testInteger: ', LData.AsInteger['testInteger']);
end;

begin
  ReportMemoryLeaksOnShutdown := True;
  try
//    Test_1;
//    Test_2;
//    Test_3;
    Test_4;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
  Readln;
end.
