unit uDataHolder;

interface

type
  IDataHolder = interface
  ['{6E89B486-4E81-48A7-B51E-38AD9194A0AF}']
    function GetValue(const AName: string): Variant;
    procedure SetValue(const AName: string; const AValue: Variant);
    function GetAsString(const AName: string): String;
    procedure SetAsString(const AName: string; const AValue: String);
    function GetAsDouble(const AName: string): Double;
    procedure SetAsDouble(const AName: string; const AValue: Double);
    function GetAsInteger(const AName: string): Integer;
    procedure SetAsInteger(const AName: string; const AValue: Integer);

    property Value[const AName: String]: Variant read GetValue write SetValue;
    property AsString[const AName: String]: String read GetAsString write SetAsString;
    property AsDouble[const AName: String]: Double read GetAsDouble write SetAsDouble;
    property AsInteger[const AName: String]: Integer read GetAsInteger write SetAsInteger;
  end;

function CreateDataHolder: IDataHolder;

implementation

uses
  System.Generics.Collections;

type
  TDataHolder = class(TInterfacedObject, IDataHolder)
  private
    type THolder = TDictionary<string,Variant>;
  private
    FHolder: THolder;
  protected
    function GetValue(const AName: string): Variant;
    procedure SetValue(const AName: string; const AValue: Variant);
    function GetAsString(const AName: string): String;
    procedure SetAsString(const AName: string; const AValue: String);
    function GetAsDouble(const AName: string): Double;
    procedure SetAsDouble(const AName: string; const AValue: Double);
    function GetAsInteger(const AName: string): Integer;
    procedure SetAsInteger(const AName: string; const AValue: Integer);
  public
    constructor Create;
    destructor Destroy; override;
  end;

function CreateDataHolder: IDataHolder;
begin
  Result := TDataHolder.Create;
end;

{ TDataHolder }

constructor TDataHolder.Create;
begin
  inherited;
  FHolder := THolder.Create;
end;

destructor TDataHolder.Destroy;
begin
  FHolder.Free;
  inherited;
end;

function TDataHolder.GetAsDouble(const AName: string): Double;
begin
  Result := FHolder[AName];
end;

function TDataHolder.GetAsInteger(const AName: string): Integer;
begin
  Result := FHolder[AName];
end;

function TDataHolder.GetAsString(const AName: string): String;
begin
  Result := FHolder[AName];
end;

function TDataHolder.GetValue(const AName: string): Variant;
begin
  Result := FHolder[AName]
end;

procedure TDataHolder.SetAsDouble(const AName: string; const AValue: Double);
begin
  FHolder.AddOrSetValue(AName, AValue);
end;

procedure TDataHolder.SetAsInteger(const AName: string; const AValue: Integer);
begin
  FHolder.AddOrSetValue(AName, AValue);
end;

procedure TDataHolder.SetAsString(const AName, AValue: String);
begin
  FHolder.AddOrSetValue(AName, AValue);
end;

procedure TDataHolder.SetValue(const AName: string; const AValue: Variant);
begin
  FHolder.AddOrSetValue(AName, AValue);
end;

end.
