unit uTeamBuilder;

interface

uses
  uTeam;

type
  TTeamBuilder = record
  private
    type
      TTeamProperties = (tpName, tpNickName, tpShirtColor, tpHomeTown, tpGroud);
    const
      TeamProperties: array[TTeamProperties] of string = ('Name', 'NickName', 'ShirtColor', 'HomeTown', 'Ground');
  strict private
    FName: string;
    FNickName: string;
    FShirtColor: TColorEnum;
    FHomeTown: string;
    FGround: string;

    function SetProperty(const AProperty: TTeamProperties{; const AValue: Variant}): TTeamBuilder;
  public
    function CreateTeam(const AName: string): TTeamBuilder;
    function WithNickName(const ANickName: string): TTeamBuilder;
    function FromTown(const AHomeTown: string): TTeamBuilder;
    function WithShirtColor(const AShirtColor: TColorEnum): TTeamBuilder;
    function PlayingAt(const AGround: string): TTeamBuilder;

    class operator Explicit(const ATeamBuilder: TTeamBuilder): TTeam;
    class operator Implicit(const ATeamBuilder: TTeamBuilder): TTeam;
    class function New: TTeamBuilder; static;

    property Name: string read FName;
    property NickName: string read FNickName;
    property ShirtColor: TColorEnum read FShirtColor;
    property HomeTown: string read FHomeTown;
    property Ground: string read FGround;

    property PName{[const AValue: Variant]}: TTeamBuilder index TTeamProperties.tpName read SetProperty;
  end;

implementation

{ TTeamBuilder }

function TTeamBuilder.CreateTeam(const AName: string): TTeamBuilder;
begin
  FName := AName;
  Result := Self;
end;

class operator TTeamBuilder.Explicit(const ATeamBuilder: TTeamBuilder): TTeam;
begin
  Result := TTeam.Create(ATeamBuilder.Name,
    ATeamBuilder.NickName, ATeamBuilder.HomeTown, ATeamBuilder.Ground, ATeamBuilder.ShirtColor);
end;

function TTeamBuilder.FromTown(const AHomeTown: string): TTeamBuilder;
begin
  FHomeTown := AHomeTown;
  Result := Self;
end;

class operator TTeamBuilder.Implicit(const ATeamBuilder: TTeamBuilder): TTeam;
begin
  Result := TTeam(ATeamBuilder);
end;

class function TTeamBuilder.New: TTeamBuilder;
begin
  Result
    .CreateTeam('')
    .WithNickName('')
    .FromTown('')
    .PlayingAt('')
    .WithShirtColor(ceNone);
end;

function TTeamBuilder.PlayingAt(const AGround: string): TTeamBuilder;
begin
  FGround := AGround;
  Result := Self;
end;

function TTeamBuilder.SetProperty(const AProperty: TTeamProperties{;
  const AValue: Variant}): TTeamBuilder;
begin

end;

function TTeamBuilder.WithNickName(const ANickName: string): TTeamBuilder;
begin
  FNickName := ANickName;
  Result := Self;
end;

function TTeamBuilder.WithShirtColor(
  const AShirtColor: TColorEnum): TTeamBuilder;
begin
  FShirtColor := AShirtColor;
  Result := Self;
end;

end.
