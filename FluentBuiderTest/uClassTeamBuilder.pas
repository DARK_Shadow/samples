unit uClassTeamBuilder;

interface

uses
  uDataHolder;

type
  IBuilder = interface;
  IPropertyFiller = interface;
  IPropertySetter = interface;
  IPropertyGetter = interface;

  IPropertySetter = interface
  ['{3DA24460-7C4C-4404-923B-3E8B80640DE2}']
    function SetValue(const AValue: Variant): IBuilder;
    function SetAsString(const AValue: string): IBuilder;
    function SetAsInteger(const AValue: Integer): IBuilder;
    function SetAsDouble(const AValue: Double): IBuilder;
  end;

  IPropertyGetter = interface
  ['{3DA24460-7C4C-4404-923B-3E8B80640DE2}']
    function GetValue(out AValue: Variant): IBuilder;
    function GetAsString(out AValue: string): IBuilder;
    function GetAsInteger(out AValue: Integer): IBuilder;
    function GetAsDouble(out AValue: Double): IBuilder;
  end;

  IPropertyFiller = interface
  ['{E37BB8F7-AB66-4B42-B444-23C783466588}']
    function Read: IPropertyGetter;
    function Write: IPropertySetter;
  end;

  IBuilder = interface(IDataHolder)
  ['{CA889A42-689F-42A4-BE64-214004171190}']
    function GetProperty(const AName: string): IPropertyFiller;

    property &Property[const AName: string]: IPropertyFiller read GetProperty;
  end;

  TBuilder = class(TInterfacedObject, {IDataHolder,} IBuilder)
  private
    FDataHolder: IDataHolder;
  protected
    function GetProperty(const AName: string): IPropertyFiller;
    function GetValue(const AName: string): Variant;
    procedure SetValue(const AName: string; const AValue: Variant);
    function GetAsString(const AName: string): String;
    procedure SetAsString(const AName: string; const AValue: String);
    function GetAsDouble(const AName: string): Double;
    procedure SetAsDouble(const AName: string; const AValue: Double);
    function GetAsInteger(const AName: string): Integer;
    procedure SetAsInteger(const AName: string; const AValue: Integer);

//    property DataHolder: IDataHolder read FDataHolder write FDataHolder implements IDataHolder;
  public
    constructor Create(const ADataHolder: IDataHolder);
  end;

function CreateBuilder(const ADataHolder: IDataHolder): IBuilder;

implementation

type
  TPropertyFiller = class(TInterfacedObject, IPropertyFiller)
  private
    FBuilder: IBuilder;
    FName: string;
  protected
    function Read: IPropertyGetter;
    function Write: IPropertySetter;
  public
    constructor Create(const AName: string; const ABuilder: IBuilder);
  end;

  TPropertySetter = class(TInterfacedObject, IPropertySetter)
  private
    FBuilder: IBuilder;
    FName: string;
  protected
    function SetValue(const AValue: Variant): IBuilder;
    function SetAsString(const AValue: string): IBuilder;
    function SetAsInteger(const AValue: Integer): IBuilder;
    function SetAsDouble(const AValue: Double): IBuilder;
  public
    constructor Create(const AName: string; const ABuilder: IBuilder);
  end;

  TPropertyGetter = class(TInterfacedObject, IPropertyGetter)
  private
    FBuilder: IBuilder;
    FName: string;
  protected
    function GetValue(out AValue: Variant): IBuilder;
    function GetAsString(out AValue: string): IBuilder;
    function GetAsInteger(out AValue: Integer): IBuilder;
    function GetAsDouble(out AValue: Double): IBuilder;
  public
    constructor Create(const AName: string; const ABuilder: IBuilder);
  end;

function CreateBuilder(const ADataHolder: IDataHolder): IBuilder;
begin
  Result := TBuilder.Create(ADataHolder);
end;

{ TPropertyFiller }

constructor TPropertyFiller.Create(const AName: string; const ABuilder: IBuilder);
begin
  inherited Create;
  FBuilder := ABuilder;
  FName := AName;
end;

function TPropertyFiller.Read: IPropertyGetter;
begin
  Result := TPropertyGetter.Create(FName, FBuilder);
end;

function TPropertyFiller.Write: IPropertySetter;
begin
  Result := TPropertySetter.Create(FName, FBuilder);
end;

{ TPropertySetter }

constructor TPropertySetter.Create(const AName: string; const ABuilder: IBuilder);
begin
  inherited Create;
  FBuilder := ABuilder;
  FName := AName;
end;

function TPropertySetter.SetAsDouble(const AValue: Double): IBuilder;
begin
  FBuilder.AsDouble[FName] := AValue;
  Result := FBuilder;
end;

function TPropertySetter.SetAsInteger(const AValue: Integer): IBuilder;
begin
  FBuilder.AsInteger[FName] := AValue;
  Result := FBuilder;
end;

function TPropertySetter.SetAsString(const AValue: string): IBuilder;
begin
  FBuilder.AsString[FName] := AValue;
  Result := FBuilder;
end;

function TPropertySetter.SetValue(const AValue: Variant): IBuilder;
begin
  FBuilder.Value[FName] := AValue;
  Result := FBuilder;
end;

{ TPropertyGetter }

constructor TPropertyGetter.Create(const AName: string;
  const ABuilder: IBuilder);
begin
  inherited Create;
  FName := AName;
  FBuilder := ABuilder;
end;

function TPropertyGetter.GetAsDouble(out AValue: Double): IBuilder;
begin
  AValue := FBuilder.AsDouble[FName];
  Result := FBuilder;
end;

function TPropertyGetter.GetAsInteger(out AValue: Integer): IBuilder;
begin
  AValue := FBuilder.AsInteger[FName];
  Result := FBuilder;
end;

function TPropertyGetter.GetAsString(out AValue: string): IBuilder;
begin
  AValue := FBuilder.AsString[FName];
  Result := FBuilder;
end;

function TPropertyGetter.GetValue(out AValue: Variant): IBuilder;
begin
  AValue := FBuilder.Value[FName];
  Result := FBuilder;
end;

{ TBuilder }

constructor TBuilder.Create(const ADataHolder: IDataHolder);
begin
  inherited Create;
  FDataHolder := ADataHolder;
end;

function TBuilder.GetAsDouble(const AName: string): Double;
begin
  Result := FDataHolder.AsDouble[AName];
end;

function TBuilder.GetAsInteger(const AName: string): Integer;
begin
  Result := FDataHolder.AsInteger[AName];
end;

function TBuilder.GetAsString(const AName: string): String;
begin
  Result := FDataHolder.AsString[AName];
end;

function TBuilder.GetProperty(const AName: string): IPropertyFiller;
begin
  Result := TPropertyFiller.Create(AName, Self);
end;

function TBuilder.GetValue(const AName: string): Variant;
begin
  Result := FDataHolder.Value[AName];
end;

procedure TBuilder.SetAsDouble(const AName: string; const AValue: Double);
begin
  FDataHolder.AsDouble[AName] := AValue;
end;

procedure TBuilder.SetAsInteger(const AName: string; const AValue: Integer);
begin
  FDataHolder.AsInteger[AName] := AValue;
end;

procedure TBuilder.SetAsString(const AName, AValue: String);
begin
  FDataHolder.AsString[AName] := AValue;
end;

procedure TBuilder.SetValue(const AName: string; const AValue: Variant);
begin
  FDataHolder.Value[AName] := AValue;
end;

end.
