unit uTeam;

interface

type
  TColorEnum = (ceNone, ceWhite, ceRed, ceGreen, ceBlue);

  TTeam = class
  private const
    ColorToString: array[TColorEnum] of string = ('None', 'White', 'Red', 'Green', 'Blue');
  strict private
    FName: string;
    FNickName: string;
    FShirtColor: TColorEnum;
    FHomeTown: string;
    FGround: string;
  public
    constructor Create(const AName, ANickName, AHomeTown, AGround: string; const AShirtColor: TColorEnum);
    function ToString: string; override;
  end;

implementation

uses
  System.SysUtils;

{ TTeam }

constructor TTeam.Create(const AName, ANickName, AHomeTown, AGround: string;
  const AShirtColor: TColorEnum);
begin
  inherited Create;
  FName := AName;
  FNickName := ANickName;
  FHomeTown := AHomeTown;
  FGround := AGround;
  FShirtColor := AShirtColor;
end;

function TTeam.ToString: string;
begin
  Result := Format('Name: %s' + sLineBreak
    + 'NickName: %s' + sLineBreak
    + 'HomeTown: %s' + sLineBreak
    + 'Ground: %s' + sLineBreak
    + 'ShirtColor: %s' + sLineBreak, [FName, FNickName, FHomeTown, FGround, ColorToString[FShirtColor]]);
end;

end.
